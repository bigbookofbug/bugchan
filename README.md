# About
A small channel i created to hold some packages that i like that are currently unavailable in the Guix package repository. i may submit some for upstreaming but others are likely to stay local to this channel.

# Usage

Add the following to your `~/.config/guix/channels.scm`
```
(channel
 (name 'bugchan)
 (url "https://gitlab.com/bigbookofbug/bugchan.git")
 (branch "master")
 (introduction
   (make-channel-introduction
   "c2ed428f3a41c1fce149b245253f9f5382f42efd"
   (openpgp-fingerprint
    "ACF7 CA25 C886 9B8A 1669 67D7 7350 3372 E2C6 3BCF"))))
```
Run `guix pull` to pull from the channel.
