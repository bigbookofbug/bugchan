(define-module (bugchan packages shell-utils-extra)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages bash)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:))

(define-public zsh-powerlevel10k
  (package
   (name "zsh-powerlevel10k")
   (version "1.20.0")
   (home-page "https://github.com/romkatv/powerlevel10k")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/romkatv/powerlevel10k")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "1ha7qb601mk97lxvcj9dmbypwx7z5v0b7mkqahzsq073f4jnybhi"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan `(("powerlevel10k.zsh-theme" "share/zsh/plugins/zsh-powerlevel10k/")
		       ("powerlevel9k.zsh-theme" "share/zsh/plugins/zsh-powerlevel10k/")
		       ("config" "share/zsh/plugins/zsh-powerlevel10k/")
		       ("gitstatus" "share/zsh/plugins/zsh-powerlevel10k/")
		       ("internal" "share/zsh/plugins/zsh-powerlevel10k/"))))
   (synopsis "A fast reimplementation of Powerlevel9k ZSH theme")
   (description "To make use of this derivation, use
      \"source\" zsh-powerlevel10k \"/share/zsh/plugins/zsh-powerlevel10k/powerlevel10k.zsh-theme\"")
   (license license:expat)))

(define-public zsh-vi-mode
  (package
   (name "zsh-vi-mode")
   (version "0.11.0")
   (home-page "https://github.com/jeffreytse/zsh-vi-mode")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/jeffreytse/zsh-vi-mode")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "0bs5p6p5846hcgf3rb234yzq87rfjs18gfha9w0y0nf5jif23dy5"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan `(("zsh-vi-mode.plugin.zsh" "share/zsh/plugins/zsh-vi-mode/")
		       ("zsh-vi-mode.zsh" "share/zsh/plugins/zsh-vi-mode/"))))
   (synopsis "A better and friendly vi(vim) mode plugin for ZSH.")
   (description "To make use of this derivation, use
      \"source\" zsh-vi-mode \"/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh\"")
   (license license:expat)))

