(define-module (bugchan packages sddm-themes)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (gnu packages qt)
  #:use-module ((guix licenses) #:prefix license:))

(define-public lain-wired-sddm-theme
  (let ((version "v0.9.1"))
    (package
     (name "lain-wired-sddm-theme")
     (version version)
     (source (origin
	      (method url-fetch)
	      (uri "https://github.com/lll2yu/sddm-lain-wired-theme/archive/refs/tags/0.9.1.tar.gz")
	      (sha256
	       (base32
		"1cqqaq6xcqg4ygwizggnbhgli3g5bli805hg295ic6djzxgwx1f0"))))
     (build-system copy-build-system)
     (arguments
      `(#:install-plan '(("." "/share/sddm/themes/sddm-lain-wired-theme"))))
     (propagated-inputs
      (list qtmultimedia-5
	    qtquickcontrols-5))
     (home-page "https://github.com/lll2yu/sddm-lain-wired-theme")
     (synopsis "A sddm login screen inspired by 1998 anime \"Serial Experiments Lain\". Requires use of the sddm-qt5 package.")
     (description "This package provides an animated theme for SDDM with aesthetics based off the anime Serial Experiments Lain")
     (license license:cc-by-sa4.0))))
lain-wired-sddm-theme
