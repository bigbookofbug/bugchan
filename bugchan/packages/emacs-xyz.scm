(define-module (bugchan packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages video)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emacs-emms-info-mediainfo
  (package
   (name "emacs-emms-info-mediainfo")
   (version "20131223.1300")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/fgallina/emms-info-mediainfo.git")
	   (commit "bce16eae9eacd38719fea62a9755225a888da59d")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "07qbbs2i05bqndr4dxb84z50wav8ffbc56f6saw6pdx6n0sw6n6n"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-emms
			    mediainfo))
   (home-page "https://github.com/fgallina/emms-info-mediainfo")
   (synopsis "Info-method for EMMS using medianfo")
   (description
    "To activate this method for getting info, use something like: (require
emms-info-mediainfo) (add-to-list emms-info-functions emms-info-mediainfo).")
   (license license:gpl3)))

(define-public emacs-cider-hydra
  (package
   (name "emacs-cider-hydra")
   (version "20190816.1121")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/clojure-emacs/cider-hydra.git")
	   (commit "c3b8a15d72dddfbc390ab6a454bd7e4c765a2c95")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0qrxja9bxx07m1ij8ly36sib901a6qhczgxsp4ap4wszy63lx93r"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-cider emacs-hydra))
   (home-page "https://github.com/clojure-emacs/cider-hydra")
   (synopsis "Hydras for CIDER")
   (description
    "This package defines some hydras (pop-up menus of commands with common prefixes)
for CIDER. For more information about CIDER, see
https://github.com/clojure-emacs/cider For more information about hydras, see
https://github.com/abo-abo/hydra Hydras serve several important purposes:
discovery, memorization, and organization. - Discovery - Grouping related
commands together under a common prefix and displaying them in a single menu
facilitates discovery. - For example, if a user wants to know about CIDER's
documentation commands, they could bring up a hydra that includes commands like
`cider-doc', `cider-javadoc', etc, some of which may be new to them. -
Memorization - Hydras serve as a memory aid for the user.  By grouping related
commands together, the user has less need to memorize every command; knowing
one, she can find the others. - Organization - The process of creating hydras
can aid in organizing code.  This gives both developers and users a better
overview of what the project can or cannot do. - Thus, each hydra is like a
section of a quick-reference card.  In fact, many of the hydras here are
inspired by the CIDER refcard:
https://github.com/clojure-emacs/cider/blob/master/doc/cider-refcard.pdf.")
   (license license:gpl3+)))

(define-public emacs-flycheck-clojure
  (package
   (name "emacs-flycheck-clojure")
   (version "20191215.2227")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/clojure-emacs/squiggly-clojure.git")
	   (commit "592c4f89efb5112784cbf94c9ea6fdd045771b62")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0yyy1c385jn0m6ql7vf9za4waqznr4mvv7fd234ygcbvhqn4pfdz"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-cider emacs-flycheck emacs-let-alist))
   (arguments
    '(#:include '("^elisp/flycheck-clojure/[^/]+.el$"
		  "^elisp/typed-clojure/[^/]+.el$")
      #:exclude '()))
   (home-page "https://github.com/clojure-emacs/squiggly-clojure")
   (synopsis "Flycheck: Clojure support")
   (description
    "Add Clojure support to Flycheck.  Provide syntax checkers to check Clojure code
using a running Cider repl.  Installation: (eval-after-load flycheck
(flycheck-clojure-setup)).")
   (license license:gpl3+)))

(define-public emacs-insert-kaomoji
  (package
   (name "emacs-insert-kaomoji")
   (version "20220215.1204")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://melpa.org/packages/insert-kaomoji-"
			  version ".tar"))
      (sha256
       (base32 "163zwvvbx7b8m2vj1r87dscdf6bapgf59fi1wlia63bnbc1gc08v"))))
    (build-system emacs-build-system)
    (arguments
     (list
	#:phases
	#~(modify-phases %standard-phases
			 (add-after 'install 'install-kaomojis
				    (lambda* (#:key inputs outputs #:allow-other-keys)
				      (let* ((out (assoc-ref outputs "out"))
					     (kaomoji-dir (string-append out
							   "/share/emacs/site-lisp/"
							   "insert-kaomoji-" #$version)))
					(copy-file "KAOMOJIS"
						   (string-append kaomoji-dir
								  "/KAOMOJIS"))))))))
    (home-page "https://git.sr.ht/~pkal/insert-kaomoji")
    (synopsis "Easily insert kaomojis")
    (description
     "Kaomojis are eastern/Japanese emoticons, which are usually displayed
horizontally, as opposed to the western vertical variants (\":^)\", \";D\", \"XP\",
...).  To achieve this they make much more use of more obscure and often harder
to type unicode symbols, which often makes it more difficult to type, or
impossible if you don't know the symbols names/numbers.  This package tries to
make it easier to use kaomojis, by using `completing-read and different
categories.  The main user functions are therefore `insert-kaomoji to insert a
kaomoji at point, and `insert-kaomoji-into-kill-ring to push a kaomoji onto the
kill ring.  The emoticons aren't stored in this file, but (usually) in the
KAOMOJIS file that should be in the same directory as this source file.  This
file is parsed during byte-compilation and then stored in
`insert-kaomoji-alist'.  The kaomojis in KAOMOJIS have been selected and
collected from these sites: -
https://wikileaks.org/ciav7p1/cms/page_17760284.html - http://kaomoji.ru/en/ -
https://en.wikipedia.org/wiki/List_of_emoticons.")
    (license license:cc0)))
emacs-insert-kaomoji
