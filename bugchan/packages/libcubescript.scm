(define-module (bugchan packages libcubescript)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:))

(define-public libcubescript
  (let ((commit "6b0a28afb6fabf097b1e0304764872b1bfe58d00"))
    (package
      (name "libcubescript")
      (version "1.0.0")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://git.octaforge.org/OctaForge/libcubescript")
	       (commit commit)))
	 (sha256
	  (base32 "0dkba3sy1lfckwcz91w1mgpmp5va4g75xj9r5idri75j7kmhdifd"))))
      (build-system meson-build-system)
      (native-inputs (list pkg-config))
      (home-page "https://git.octaforge.org/OctaForge/libcubescript")
      (synopsis "An embeddable, thread-safe implementation of the cubescript language")
      (description "Cubescript is a minimal scripting language first introduced in the Cube FPS and carried over into derived games and game engines such as Sauerbraten. Originally being little more than a few hundred lines of code, serving primarily as the console and configuration file format of the game, it grew more advanced features as well as a bytecode VM.

Nowadays, it is a minimal but relatively fully featured scripting language based around the concept that everything can be interpreted as a string. It excels at its original purpose as well as things like text preprocessing. It comes with a Lisp-like syntax and a variety of standard library functions.

Libcubescript is a project that aims to provide an independent, improved, separate implementation of the language, available as a library, intended to satisfy the needs of the OctaForge project. It was originally forked from Cubescript as present in the Tesseract game/engine and gradually rewritten; right now, very little of the original code remains. At language level it is mostly compatible with the other implementations (although with a stricter parser and extra features), while the standard library does not aim to be fully compatible. Some features are also left up to the user to customize, so that it is not tied to game engines feature-wise.

Like the codebase it is derived from, it is available under the permissive zlib license, and therefore compatible with just about anything.")
      (license license:zlib))))
