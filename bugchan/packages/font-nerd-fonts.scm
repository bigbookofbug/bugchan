(define-module (bugchan packages font-nerd-fonts)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system font)
  #:use-module ((guix licenses) #:prefix license:))

(define-public font-0xproto-nerd-font
(let ((version "v3.2.1"))
  (package
   (name "font-0xproto-nerd-font")
   (version version)
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/0xProto.tar.xz"))
	    (sha256
		 (base32
		       "09q4ipl3vvav3jbs0s14fqmd2wk70mc7i8mmplpj20jwcicm05ng"))))
   (build-system font-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'install 'make-files-writable
             (lambda _
               (for-each
                make-file-writable
                (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
               #t)))))
   (home-page "https://www.nerdfonts.com/")
   (synopsis "Iconic font aggregator, collection, and patcher")
   (description
    "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
   (license license:silofl1.1))))

(define-public font-3270-nerd-font
  (let ((version "v3.2.1"))
    (package
      (name "font-3270-nerd-font")
      (version version)
      (source (origin
		(method url-fetch)
		(uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/3270.tar.xz"))
		(sha256
		 (base32
		  "0zcj5xhylyqxpwn6dzp28kh8xybhh4y9lva3smcqs7iklhbf8s31"))))
      (build-system font-build-system)
      (arguments
       `(#:phases
	 (modify-phases %standard-phases
	   (add-before 'install 'make-files-writable
	     (lambda _
	       (for-each
		make-file-writable
		(find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
	       #t)))))
      (home-page "https://www.nerdfonts.com/")
      (synopsis "Iconic font aggregator, collection, and patcher")
      (description
       "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
      (license license:cc-by-sa3.0))))

(define-public font-agave-nerd-font
  (let ((version "v3.2.1"))
    (package
      (name "font-agave-nerd-font")
      (version version)
      (source (origin
		(method url-fetch)
		(uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/agave.tar.xz"))
		(sha256
		 (base32
		  "1xxylrjb0zbq3kj14fx5d3lpb7abl0br6mkj961i391qyqlx01y3"))))
      (build-system font-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'install 'make-files-writable
             (lambda _
               (for-each
                make-file-writable
                (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
               #t)))))
      (home-page "https://www.nerdfonts.com/")
      (synopsis "Iconic font aggregator, collection, and patcher")
      (description
       "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
      (license license:expat))))

(define-public font-anonymouspro-nerd-font
(let ((version "v3.2.1"))
  (package
    (name "font-anonymouspro-nerd-font")
    (version version)
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/anonymouspro.tar.xz"))
	      (sha256
	       (base32
		"1lq9wkf8c153jkjmhnddih12y9xd4gabbkgac0vx9j98hmy7yjqy"))))
    (build-system font-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
	 (add-before 'install 'make-files-writable
	   (lambda _
	     (for-each
	      make-file-writable
	      (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
	     #t)))))
    (home-page "https://www.nerdfonts.com/")
    (synopsis "Iconic font aggregator, collection, and patcher")
    (description
     "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
    (license license:silofl1.1))))

(define-public font-arimo-nerd-font
  (let ((version "v3.2.1"))
    (package
      (name "font-arimo-nerd-font")
      (version version)
      (source (origin
		(method url-fetch)
		(uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/arimo.tar.xz"))
		(sha256
		 (base32
		  "1f7zljmljrp1dxkrhkyz8zh9ddv7l7m9br6gaygzxr26bq0vrwr0"))))
      (build-system font-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'install 'make-files-writable
             (lambda _
               (for-each
                make-file-writable
                (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
               #t)))))
      (home-page "https://www.nerdfonts.com/")
      (synopsis "Iconic font aggregator, collection, and patcher")
      (description
       "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
      (license license:asl2.0))))

(define-public font-aurulentsansmono-nerd-font
  (let ((version "v3.2.1"))
    (package
      (name "font-aurulentsansmono-nerd-font")
      (version version)
      (source (origin
		(method url-fetch)
		(uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/aurulentsansmono.tar.xz"))
		(sha256
		 (base32
		  "0wlwwgp1w7rqvqx66dkqwhz5flw75620fj9fb795hakpkjiya6yp"))))
      (build-system font-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'install 'make-files-writable
             (lambda _
               (for-each
                make-file-writable
                (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
               #t)))))
      (home-page "https://www.nerdfonts.com/")
      (synopsis "Iconic font aggregator, collection, and patcher")
      (description
       "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
      (license license:silofl1.1))))

;(define-public font-bigblueterminal-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-bigblueterminal-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/BigBlueTerminal.tar.xz"))
;	    (sha256
;		 (base32
;		       "115cxnll1iyj75f5wi7b7pi5hgfa3b5kbx269alm9183h284lb23"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-bitstreamverasansmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-bitstreamversansmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/BitstreamVeraSansMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "1s6jpg0vrdwgi9qyn0mbcy8r7h1lqw8z6q39wiin61szfn642a2k"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-cascadiacode-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-cascadiacode-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/CascadiaCode.tar.xz"))
;	    (sha256
;		 (base32
;		       "1sg6czl3km7yi70vdcyb0ff1xkq1p4nalj0yh164gan3psp9mxss"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-cascadiamono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-cascadiamono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/CascadiaMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "1p35nmzl51mn2mk0g9fdcawvssv4v7bklxxmdx99357ihnlka26w"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-codenewroman-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-codenewroman-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/CodeNewRoman.tar.xz"))
;	    (sha256
;		 (base32
;		       "0p1wkmpzcrxw8qv5lf9fwsxqpjglhwim83amf7i8mmxdx1drzlj5"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-comicshannsmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-comicshannsmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/ComicShannsMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "1p35nmzl51mn2mk0g9fdcawvssv4v7bklxxmdx99357ihnlka26w"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-commitmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-commitmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/commitmono.tar.xz"))
;	    (sha256
;		 (base32
;		       "0jjzi98i28s3z2x8v7rakcdhgdf7jxzagj8snrylw2mvwn7mlgqp"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-cousine-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-cousine-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/cousine.tar.xz"))
;	    (sha256
;		 (base32
;		       "1ccq3sp8fqbi0njm9w2p6cf5r7avpan5fklwzpx4mknwbdqlrwdq"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-d2coding-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-d2coding-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/D2Coding.tar.xz"))
;	    (sha256
;		 (base32
;		       "1c0chgbsmzlgq0vinbcz7ydkdhnram8cif8zx8kkpha31abna5n0"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-daddytimemono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-daddytimemono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/DaddyTimeMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "1nz4g26a4dx7ng5nv6bc4hg474inan1c00c86mdlmvl2vgmx6zbf"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-dejavusansmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-dejavusansmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/DejaVuSansMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "12x18i15723hxc1l6ng92m77wycjk0d6r15j34n1zsww5r6xwjxr"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-droidsansmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-droidsansmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/DroidSansMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "1mqd1qqbs9dxwmi98i4xw88c68l1bww1sqlsmxmn86368rjh14fk"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-envycoder-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-envycoder-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/EnvyCodeR.tar.xz"))
;	    (sha256
;		 (base32
;		       "197g4jaljcb1yncn9rvh17n077p7bq0v59lvb9vqkq3lms5lzjni"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-fantasquesansmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-fantasquesansmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/FantasqueSansMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "079mk8xrri4r1d5k1w5fv1hb0hp3w499csirkd6yriss35sbhv9d"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-firacode-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-firacode-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/FiraCode.tar.xz"))
;	    (sha256
;		 (base32
;		       "1i1vw65f00n6vjinyqr1bq5ni5r6g8cjinrfl5zhlqm0gagv5x6y"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "Iconic font aggregator, collection, and patcher")
;   (description
;    "Nerd Fonts patches developer targeted fonts with a high number
;of glyphs (icons). Specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-firamono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-firamono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/firamono.tar.xz"))
;	    (sha256
;		 (base32
;		       "1i9bfxblx568wsjq7ks1kyyfn9k36i4r2an4n45mb46swc94n8n0"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-geistmono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-geistmono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/GeistMono.tar.xz"))
;	    (sha256
;		 (base32
;		       "0wvc9hqkh7ap3ysklcin8k72706l72p2wyqv3bg5brzcmwcpy90g"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-go-mono-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-go-mono-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/Go-Mono.tar.xz"))
;	    (sha256
;		 (base32
;		       "0xiwzdchmhad2lqb2xf1bcfhksn5bwdjn041m0lwzzw5lnks1yc7"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-gohu-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-gohu-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/Gohu.tar.xz"))
;	    (sha256
;		 (base32
;		       "0h8hjiqs3f6xwn7g4wg12xim65ybw2546nlf5p9ip4ymr7x17dks"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-hack-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-hack-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/hack.tar.xz"))
;	    (sha256
;		 (base32
;		       "1wxmd4jr4p11cfhzs5chyh649vps6sdz4bq28204npkd7wzh5fc9"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-hasklig-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-hasklig-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/hasklig.tar.xz"))
;	    (sha256
;		 (base32
;		       "1nja4r8sn67g3gn85xhb1h7p1pi96wl0hpg5b5gyd1z5llbgzc2g"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-heavydata-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-heavydata-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/heavydata.tar.xz"))
;	    (sha256
;		 (base32
;		       "1a3a1pixv97wlnai24zb8dhkzxb2llcarhjkfrgd4syhn37sdf7n"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
;(define-public font-hermit-nerd-font
;(let ((version "v3.2.1"))
;  (package
;   (name "font-hermit-nerd-font")
;   (version version)
;   (source (origin
;	    (method url-fetch)
;	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/hermit.tar.xz"))
;	    (sha256
;		 (base32
;		       "1bh18rzwma7gzrx3ybw8g2s9k5xv7wx8ybnyas3qaxb03fpjqm93"))))
;   (build-system font-build-system)
;   (home-page "https://www.nerdfonts.com/")
;   (synopsis "iconic font aggregator, collection, and patcher")
;   (description
;    "nerd fonts patches developer targeted fonts with a high number
;of glyphs (icons). specifically to add a high number of extra glyphs
;from popular ‘iconic fonts’ such as font awesome, devicons, octicons,
;and others.")
;	    (license license:expat))))
;
(define-public font-jetbrainsmono-nerd-font
(let ((version "v3.2.1"))
  (package
   (name "font-jetbrainsmono-nerd-font")
   (version version)
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ryanoasis/nerd-fonts/releases/download/" version "/JetBrainsMono.tar.xz"))
	    (sha256
		 (base32
		       "01j0rkgrix7mdp9fx0y8zzk1kh40yfcp932p0r5y666aq4mq5y3c"))))
   (build-system font-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'install 'make-files-writable
             (lambda _
               (for-each
                make-file-writable
                (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
               #t)))))
   (home-page "https://www.nerdfonts.com/")
   (synopsis "Iconic font aggregator, collection, and patcher")
   (description
    "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
	    (license license:silofl1.1))))

