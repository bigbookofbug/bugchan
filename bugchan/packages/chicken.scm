(define-module (bugchan packages chicken)
  #:use-module (gnu packages chicken)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system chicken)
  #:use-module ((guix licenses) #:prefix license:))

(define-public chicken-json
  (package
   (name "chicken-json")
   (version "1.6")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "json" version))
	   (sha256
	    (base32
	     "1h27h6z7awadijk9w51df9dyk3wma27fp180j7xwl27sbw7h76wz"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "json"))
   (propagated-inputs
    (list chicken-packrat chicken-srfi-69 chicken-srfi-1))
   (home-page "https://wiki.call-cc.org/eggref/5/json")
   (synopsis "A json library")
   (description "This egg implements a parser and generator for the JSON data interchange format.")
   (license license:expat)))

(define-public chicken-miscmacros
  (package
   (name "chicken-miscmacros")
   (version "1.0")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "miscmacros" version))
	    (sha256
	     (base32
	      "0xs8ksnbpxm0a1s2bcqybliaqpr5agin4ksn3hyjwniqhzx4qzg8"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "miscmacros"))
   (home-page "https://wiki.call-cc.org/eggref/5/miscmacros")
   (synopsis "Various helper macros")
   (description "This egg provides various useful, little macros")
   (license license:bsd-3)))

(define-public chicken-srfi-71
  (package
   (name "chicken-srfi-71")
   (version "0.2")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "srfi-71" version))
	    (sha256
	    (base32
	     "0v10ylzd5r61l2f6b2v3j4mpxf50pwmrlaqrhx66bw2fps3n32qp"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "srfi-71"))
   (propagated-inputs
    (list chicken-srfi-69 chicken-srfi-1 chicken-miscmacros))
   (native-inputs
    (list chicken-test))
   (home-page "https://wiki.call-cc.org/eggref/5/srfi-71#srfi-71")
   (synopsis "SRFI-71 record types.")
   (description "This egg consists of a single library, srfi-71, that provides a number of public modules.")
   (license license:bsd-3)))

(define-public chicken-srfi-99
  (package
   (name "chicken-srfi-99")
   (version "1.4.5")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "srfi-99" version))
	    (sha256
	     (base32
	      "033hid04aaph0xmsc68r417dnjyswfqyd20y5cl4q3v8izqi54ks"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "srfi-99"))
   (propagated-inputs
    (list chicken-srfi-69 chicken-srfi-1 chicken-miscmacros))
   (native-inputs
    (list chicken-test))
   (home-page "https://wiki.call-cc.org/eggref/5/srfi-99#srfi-99")
   (synopsis "SRFI-99 record types.")
   (description "This egg consists of a single library, srfi-99, that provides a number of public modules.")
   (license license:bsd-3)))

(define-public chicken-packrat
  (package
   (name "chicken-packrat")
   (version "1.5")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "packrat" version))
	   (sha256
	    (base32
	     "0d7ly5zvswg07gzm504min730qy16yafz3acyq45smd7q52s47fp"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "packrat"))
   (propagated-inputs
    (list chicken-srfi-1))
   (home-page "https://wiki.call-cc.org/eggref/5/packrat")
   (synopsis "Packrat parsing library")
   (description "Packrat parsing is a memoizing, backtracking recursive-descent parsing technique that runs in time and space linear in the size of the input text. The technique was originally discovered by Alexander Birman in 1970 , and Bryan Ford took up the idea for his master's thesis in 2002.")
   (license license:expat)))

(define-public chicken-queues
  (package
   (name "chicken-queues")
   (version "0.1")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "queues" version))
	   (sha256
	    (base32
	     "0i7ywz4p5fhninfgf0fk8h6cdqdp4hdqb76y5cgnzspnmv5qpg26"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "queues"))
   (home-page "https://wiki.call-cc.org/eggref/5/queues")
   (synopsis "A queue data structure")
   (description "This egg provides a single-ended queue data structure.")
   (license license:public-domain)))

(define-public chicken-shell
  (package
   (name "chicken-shell")
   (version "0.4")
   (source (origin
	    (method url-fetch)
	    (uri (egg-uri "shell" version))
	   (sha256
	    (base32
	     "04gn93nsf3g8bxd7jb498qr629y7ql6j548s4c46wfwc5f4gjn5c"))))
   (build-system chicken-build-system)
   (arguments '(#:egg-name "shell"))
   (home-page "https://wiki.call-cc.org/eggref/5/shell")
   (synopsis "TODO")
   (description "TODO")
   (license license:public-domain)))

