(define-module (bugchan packages xdisorg)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xdisorg) ;;remove when merging
  #:use-module (guix packages)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages fontutils)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))

;;the 1.3.5 version does not work on wayland, thus windows-alpha is used here
;;despite the name "alpha", its stable on linux
(define-public warpd-wayland
  (package
    (name "warpd-wayland")
    (version "1.3.7-windows-alpha")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/rvaiya/warpd")
	     (commit (string-append "v" version))))
       (sha256
	(base32 "0al0saq9zlp2ckvn45xviaa5cszpzabar4l5svqmd6imf6rlh1x4"))))
    (build-system gnu-build-system)
    (inputs
     (list cairo wayland libxkbcommon))
    (arguments `(#:make-flags (list (string-append "CC=" ,(cc-for-target))
			       (string-append "PREFIX=" %output)
				    "DISABLE_X=y")
		 #:tests? #f
		 #:phases (modify-phases %standard-phases
			    (delete 'configure))))
    (home-page "https://github.com/rvaiya/warpd")
    (synopsis "TODO")
    (description "TODO")
    (license license:expat)))

;;currently unable to build for x due to missing "ft2build.h" file
;;maybe check py-wlroots for an idea of how to remedy this
;(define-public warpd
;  (package
;    (name "warpd-wayland")
;    (version "1.3.7-windows-alpha")
;    (source
;     (origin
;       (method git-fetch)
;       (uri (git-reference
;	     (url "https://github.com/rvaiya/warpd")
;	     (commit (string-append "v" version))))
;       (sha256
;	(base32 "0al0saq9zlp2ckvn45xviaa5cszpzabar4l5svqmd6imf6rlh1x4"))))
;    (build-system gnu-build-system)
;    (native-inputs
;     (list pkg-config))
;    (inputs
;     (list cairo wayland libxfont libxkbcommon libxi libxinerama libxfixes libxtst libx11 libxft))
;    (arguments `(#:make-flags (list (string-append "CC=" ,(cc-for-target))
;			       (string-append "PREFIX=" %output))
;				   ; "DISABLE_X=y")
;		 #:phases (modify-phases %standard-phases
;			    (delete 'configure)
;			    (delete 'check))))
;    (home-page "https://github.com/rvaiya/warpd")
;    (synopsis "TODO")
;    (description "TODO")
;    (license license:expat)))
