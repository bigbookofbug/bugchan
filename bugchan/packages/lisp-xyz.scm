(define-module (bugchan packages lisp-xyz))

;; TODO -- add memory-regions

(use-modules (guix packages)
             (guix git-download)
             (guix build-system asdf)
             (guix build-system copy)
             ((guix licenses)
              #:prefix license:)
             (guix gexp)
             (gnu packages base)
             (gnu packages tls)
             (gnu packages version-control)
             (gnu packages gnupg)
             (gnu packages lisp)
             (gnu packages commencement)
             (gnu packages lisp-check)
             (gnu packages lisp-xyz))

 (define-public sbcl-damn-fast-priority-queue
   (let ((commit "0f0bcf8be05dec33d962768d52e27e45da073f9f")
	 (revision "0"))
     (package
       (name "sbcl-damn-fast-priority-queue")
       (version (git-version "0.0.0" revision commit))
       (source
	(origin
	  (method git-fetch)
	  (uri (git-reference
		(url "https://github.com/phoe/damn-fast-priority-queue")
		(commit commit)))
	  (file-name (git-file-name "cl-manifolds" version))
	  (sha256
	   (base32 "1mbigpgi7qbqvpj59l1f7p2qcg00ybvqzdca1j1b9hx62h224ndw"))))
      (build-system asdf-build-system/sbcl)
       (inputs
	(list sbcl-alexandria))
      (arguments
       `(;; TODO - fix
	 #:tests? #f))
       (home-page "TODO")
       (synopsis "TODO")
       (description
	"TODO")
       (license license:zlib))))

 (define-public sbcl-manifolds
   (let ((commit "4653bd8fb936854c570ce71aacd5f3a0d18f6dcc")
	 (revision "0"))
     (package
       (name "sbcl-manifolds")
       (version (git-version "0.0.0" revision commit))
       (source
	(origin
	  (method git-fetch)
	  (uri (git-reference
		(url "https://github.com/Shirakumo/manifolds")
		(commit commit)))
	  (file-name (git-file-name "cl-manifolds" version))
	  (sha256
	   (base32 "1q6nkq13amih3m85ckica2g4cmnnvripsdkaaxbj8nicv755g51p"))))
      (build-system asdf-build-system/sbcl)
       (inputs
	(list sbcl-3d-spaces
	      sbcl-3d-math
	      sbcl-documentation-utils))
      (arguments
       `(;; TODO - fix
	 #:tests? #f))
       (home-page "TODO")
       (synopsis "TODO")
       (description
	"TODO")
       (license license:zlib))))

 (define-public sbcl-lru-cache
   (let ((commit "a43747f527b5e263b049f4774bec9949fd92a2ca")
	 (revision "0"))
     (package
       (name "sbcl-lru-cache")
       (version (git-version "0.0.0" revision commit))
       (source
	(origin
	  (method git-fetch)
	  (uri (git-reference
		(url "https://github.com/Shinmera/lru-cache")
		(commit commit)))
	  (file-name (git-file-name "cl-manifolds" version))
	  (sha256
	   (base32 "035pl11j1l129akgf33w5c0b8c6gxw1xpj54r0fzxz3dw7cs8pg1"))))
      (build-system asdf-build-system/sbcl)
       (inputs
	(list sbcl-documentation-utils))
      (arguments
       `(;; TODO - fix
	 #:tests? #f))
       (home-page "TODO")
       (synopsis "TODO")
       (description
	"TODO")
       (license license:zlib))))

 (define-public sbcl-quickhull
   (let ((commit "f128f178a659ca8e780c9afac390888d4ea5cf02")
	 (revision "0"))
     (package
       (name "sbcl-quickhull")
       (version (git-version "0.0.0" revision commit))
       (source
	(origin
	  (method git-fetch)
	  (uri (git-reference
		(url "https://github.com/Shirakumo/quickhull")
		(commit commit)))
	  (file-name (git-file-name "cl-manifolds" version))
	  (sha256
	   (base32 "1814qq23dg2shnfdkw9w9ap53qzg2igy119bwslvflmcb1jd7bpm"))))
      (build-system asdf-build-system/sbcl)
       (inputs
	(list sbcl-3d-math
	      sbcl-documentation-utils))
      (arguments
       `(;; TODO - fix
	 #:tests? #f))
       (home-page "TODO")
       (synopsis "TODO")
       (description
	"TODO")
       (license license:zlib))))

(define-public sbcl-cl-wavefront
  (let ((commit "8de1347c7ad4ebe94c90202ae3f28328ee54251f")
	(revision "0"))
    (package
      (name "sbcl-cl-wavefront")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/Shirakumo/cl-wavefront")
	       (commit commit)))
	 (file-name (git-file-name "cl-wavefront" version))
	 (sha256
	  (base32 "1il5i04x2ff3pnjm2pgvq0hryd9rnjdbczvinj3l3w30lj553g83"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-alexandria
	     sbcl-documentation-utils
	     sbcl-parse-float
	     sbcl-cl-ppcre))
      (arguments
       `(;; TODO - fix
	 #:tests? #f))
      (home-page "TODO")
      (synopsis "TODO")
      (description
       "TODO")
      (license license:zlib))))


 (define-public sbcl-convex-covering
   (let ((commit "3be6a247a9a9604ddc36d59162a37385712199f0")
	 (revision "0"))
     (package
       (name "sbcl-convex-covering")
       (version (git-version "0.0.0" revision commit))
       (source
	(origin
	  (method git-fetch)
	  (uri (git-reference
		(url "https://github.com/Shirakumo/convex-covering")
		(commit commit)))
	  (file-name (git-file-name "cl-convex-covering" version))
	  (sha256
	   (base32 "0pw2gjiil6iriii07k2vwh0rad4vikzp0908vc5gxyjdpfrsfvld"))))
      (build-system asdf-build-system/sbcl)
      (arguments
       `(;; TODO - fix
	 #:tests? #f))
       (inputs
	(list sbcl-3d-spaces
	      sbcl-alexandria
	      sbcl-cl-wavefront
	      sbcl-cl-dot
	      sbcl-damn-fast-priority-queue
	      sbcl-inferior-shell
	      sbcl-lparallel
	      sbcl-machine-state
	      sbcl-manifolds
	      sbcl-parachute
	      sbcl-quickhull))
       (home-page "TODO")
       (synopsis "TODO")
       (description
	"TODO")
       (license license:zlib))))

; (define-public sbcl-trial-1-6
;   (let ((commit "429ca934fb9bc1696281e2bf0b18060ed87914b8")
;	 (revision "2"))
;     (package
;       (inherit sbcl-trial)
;       (version (git-version "1.6.0" revision commit))
;       (source
;	(origin
;	  (method git-fetch)
;	  (uri (git-reference
;		(url "https://github.com/Shirakumo/trial")
;		(commit commit)))
;	  (sha256
;	   (base32 "0ci422qv6rbw05p7kkkv72cj7j6kbnd1mk7sjvwjsb7rkhd777zy"))))
;       (inputs
;	(list sbcl-3d-matrices
;	      sbcl-3d-quaternions
;	      sbcl-3d-transforms
;	      sbcl-3d-vectors
;	      sbcl-3d-math
;	      sbcl-3d-spaces
;	      sbcl-alexandria
;	      sbcl-alloy
;	      sbcl-atomics
;	      sbcl-bordeaux-threads
;	      sbcl-cl-gamepad
;	      sbcl-cl-glfw3
;	      sbcl-cl-gltf
;	      sbcl-cl-jpeg
;	      sbcl-cl-opengl
;	      sbcl-cl-ppcre
;	      sbcl-cl-tga
;	      sbcl-classimp
;	      sbcl-closer-mop
;	      sbcl-convex-covering
;	      sbcl-deploy
;	      sbcl-depot
;	      sbcl-dns-client
;	      sbcl-fast-io
;	      sbcl-file-notify
;	      sbcl-filesystem-utils
;	      sbcl-flare
;	      sbcl-float-features
;	      sbcl-flow
;	      sbcl-for
;	      sbcl-form-fiddle
;	      sbcl-glop
;	      sbcl-glsl-toolkit
;	      sbcl-harmony
;	      sbcl-ieee-floats
;	      sbcl-jzon
;	      sbcl-lambda-fiddle
;	      sbcl-language-codes
;	      sbcl-lru-cache
;	      sbcl-lquery
;	      sbcl-messagebox
;	      sbcl-mmap
;	      sbcl-pathname-utils
;	      sbcl-pngload
;	      sbcl-promise
;	      sbcl-retrospectiff
;	      sbcl-sdl2
;	      sbcl-simple-tasks
;	      sbcl-static-vectors
;	      sbcl-system-locale
;	      sbcl-terrable
;	      sbcl-trivial-extensible-sequences
;	      sbcl-trivial-garbage
;	      sbcl-trivial-indent
;	      sbcl-trivial-main-thread
;	      sbcl-verbose
;	      sbcl-zpng)))))
