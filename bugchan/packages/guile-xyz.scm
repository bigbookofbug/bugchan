(define-module (bugchan packages guile-xyz)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (srfi srfi-1))

(define-public guile-guix-graze
  (let ((version "0.2"))
    (package
      (name "graze")
      (version version)
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://gitlab.com/bigbookofbug/guix-graze")
	       (commit version)))
	 (file-name (git-file-name name version))
	 (sha256
	  (base32 "0gy2ql9y0hg71p69b6iz7fy5kfp6rwn77kg5n99zb29mmabpcndb"))))
      (build-system gnu-build-system)
      (arguments
       (list
	#:modules
          `(((guix build guile-build-system)
	     #:select
	     (target-guile-effective-version))
	    ,@%default-gnu-imported-modules)
	  #:phases
          (with-imported-modules
            `((guix build guile-build-system)
              ,@%default-gnu-imported-modules)
            #~(modify-phases
                    %standard-phases
                    (add-after 'install 'graze-wrap-binaries
		      (lambda* (#:key inputs #:allow-other-keys)
			(let* ((version (target-guile-effective-version))
			       (site-ccache
				(string-append "/lib/guile/" version "/site-ccache"))
			       (site (string-append "/share/guile/site/" version))
			       (dep-path
				(lambda (env path)
				  (list env ":" 'prefix
					(cons (string-append
					       #$output
					       path)
					      (map (lambda (input)
						     (string-append
						      (assoc-ref inputs input)
						      path))
						   (list "guile-config"))))))
			       (bin (string-append (ungexp output) "/bin/")))
			  (wrap-program
			      (string-append bin "graze")
			    (dep-path "GUILE_LOAD_PATH" site)
			    (dep-path "GUILE_LOAD_COMPILED_PATH" site-ccache)))))))))
      (native-inputs
       (list autoconf automake pkg-config guile-3.0))
      (inputs (list bash-minimal guile-3.0))
      (propagated-inputs (list guile-config))
      (synopsis "A guix shell templating and automation system")
      (description
       "Graze is a command-line tool used for complex guix shell invocations, where a simple manifest does not suffice. Through the use of a shell.scm file, it allows for a declarative approach to Guix development environments, and a project templating system inspired by Nix's flake templates.")
      (home-page
   "https://gitlab.com/bigbookofbug/guix-graze")
      (license license:gpl3+))))
