(define-module (bugchan packages wm)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system zig)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages python)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages base)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages zig-xyz)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages time)
  #:use-module (gnu packages man)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages haskell)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages check)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages freedesktop)
  #:use-module (guix inferior)
  #:use-module (guix utils)
  #:use-module (guix channels)
  #:use-module (srfi srfi-1)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages wm))


(define-public python-pyflakes-2-5
  (package
    (name "python-pyflakes")
    (version "2.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pyflakes" version))
       (sha256
        (base32 "18rf67ddmjsr76w1767mil3i7vh7vzlcph58cb2wqj6a1l1fn7s9"))))
    (build-system pyproject-build-system)
    (home-page "https://github.com/PyCQA/pyflakes")
    (synopsis "passive checker of Python programs")
    (description "passive checker of Python programs.")
    (license license:expat)))

(define-public python-pycodestyle-2-9
  (package
    (name "python-pycodestyle")
    (version "2.9.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pycodestyle" version))
       (sha256
        (base32 "1hmc77w20wqh3x7ccrni46i5hsghacjldjciwxdpi7gq052s9axy"))))
    (build-system pyproject-build-system)
    (arguments `(#:tests? #f))
    (home-page "https://pycodestyle.pycqa.org/")
    (synopsis "Python style guide checker")
    (description "Python style guide checker.")
    (license license:expat)))

(define-public python-mccabe-0-7
  (package
    (name "python-mccabe")
    (version "0.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "mccabe" version))
       (sha256
        (base32 "09b34c7jj2a0yya7fp3x7lncna4zj7pr4caj9vgvnq1vqd0053il"))))
    (arguments `(#:tests? #f))
    (build-system pyproject-build-system)
    (home-page "https://github.com/pycqa/mccabe")
    (synopsis "McCabe checker, plugin for flake8")
    (description "@code{McCabe} checker, plugin for flake8.")
    (license license:expat)))

(define-public python-flake8-5
  (package
    (inherit python-flake8)
    (name "python-flake8")
    (version "5.0.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "flake8" version))
              (sha256
               (base32
                "17jh8ai2riwpzi917s7sg7abixml3rdnj2x7k19fb2aig6v0cfsh"))))
    (arguments
     `(#:tests? #f
       #:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (when tests?
                        (invoke "pytest" "-v")))))))
    (propagated-inputs
     (list python-entrypoints
           python-mccabe-0-7
           python-pycodestyle-2-9
           python-pyflakes-2-5))
    ))

(define-public python-pep8-naming-0-14-1
  (package
   (inherit python-pep8-naming)
   (name "python-pep8-naming")
    (version "0.14.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pep8-naming" version))
       (sha256
        (base32 "0dlapswhvzkkyw9gmjngc4rbzasdxnpxwj8mdkmmfmc7h2p2iwhy"))))
    (propagated-inputs (list python-flake8-5))))

(define-public python-flake8-pyproject
  (package
    (name "python-flake8-pyproject")
    (version "1.2.3")
    (source
     (origin
       (method git-fetch)               ;no tests in PyPI release
       (uri (git-reference
             (url "https://github.com/john-hen/Flake8-pyproject")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "1y38mdzjkrrvp59dcwh7cyc5askfl8w6cdgayn46x6jqpf7lix3c"))))
    (arguments
     (list
      #:tests? #f
      #:phases
      #~(modify-phases %standard-phases
          (replace 'build
            (lambda _
              (setenv "SETUPTOOLS_SCM_PRETEND_VERSION" #$version)
              ;; ZIP does not support timestamps before 1980.
              (setenv "SOURCE_DATE_EPOCH" "315532800")
              (invoke "python" "-m" "build" "--wheel" "--no-isolation" "."))))))
    (build-system pyproject-build-system)
    (native-inputs (list python-flit-core))
    (propagated-inputs (list python-flake8-5
			     python-tomli
			     python-pytest
			     python-pytest-cov))
    (home-page "https://github.com/john-hen/Flake8-pyproject")
    (synopsis "Asyncio frontend for pulsectl, a Python bindings library for PulseAudio (libpulse)")
    (description "This library provides an Python 3 asyncio interface on top of the pulsectl library for monitoring and controlling the PulseAudio sound server.")
    (license license:expat)))

(define-public python-pulsectl-asyncio
  (package
    (name "python-pulsectl-asyncio")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pulsectl_asyncio" version))
       (sha256
	(base32 "1zmz3d5qpf2dzjvhwrjkg1kbkrsdfh234q4gby5r0rqkzqqkk6g2"))))
    (arguments
     (list
      #:tests? #f))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-pulsectl))
    (home-page "https://github.com/mhthies/pulsectl-asyncio")
    (synopsis "Asyncio frontend for pulsectl, a Python bindings library for PulseAudio (libpulse)")
    (description "This library provides an Python 3 asyncio interface on top of the pulsectl library for monitoring and controlling the PulseAudio sound server.")
    (license license:expat)))

(define-public python-pywayland
  (package
    (name "python-pywayland")
    (version "0.4.18")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pywayland" version))
       (sha256
	(base32 "0cv5aqn23id31mn95q6isn5vcnjcd4dvaqzn52ihbb9sg01dx2jr"))))
    (build-system pyproject-build-system)
    (arguments
     (list
      #:tests? #f))
    (inputs (list wayland libxcb))
    (propagated-inputs (list python-cffi))
    (native-inputs (list python-pytest pkg-config))
    (home-page "https://github.com/flacjacket/pywayland/")
    (synopsis "Python bindings for the libwayland librar")
    (description "PyWayland provides a wrapper to the libwayland library using the CFFI library to provide access to the Wayland library calls and written in pure Python.")
    (license license:asl2.0)))

(define-public python-xkbcommon
  (package
    (name "python-xkbcommon")
    (version "1.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "xkbcommon" version))
       (sha256
	(base32 "0dnwbp8rriwkmsa8a40cpvrccjy7m8xz6jw0vbcka7gnvc44h5xc"))))
    (build-system pyproject-build-system)
    (inputs (list libxkbcommon libxcb))
    (propagated-inputs (list python-cffi))
    (home-page "https://github.com/sde1000/python-xkbcommon")
    (synopsis "Python bindings for libxkbcommon using cffi")
    (description "This package provides Python bindings for libxkcommon using python-cffi.")
    (license license:expat)))

(define-public python-pywlroots
  (package
   (name "python-pywlroots")
   (version "0.17.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pywlroots" version))
     (sha256
      (base32 "1frxqkkh2867rh0c6j0jsmzrga8k6235f6ygkk4cph2883hjpjvj"))))
   (build-system pyproject-build-system)
   (arguments
    (list
     #:tests? #f
     #:phases
     #~(modify-phases %standard-phases
		      (add-before 'build 'pre-build
				  ;;needed, otherwise pixman.h will not be found
				  (lambda* (#:key inputs #:allow-other-keys)
				    (let ((pixman (string-append
						   (assoc-ref inputs "pixman")
						   "/include")))
				      (setenv "C_INCLUDE_PATH"
					      (string-append pixman "/" "pixman-1" ":"
							     (or (getenv "C_INCLUDE_PATH")
								 "")))))))))
   (inputs
    (list pixman wayland libinput libxkbcommon wlroots))
   (propagated-inputs (list python-cffi python-pywayland python-xkbcommon))
   (native-inputs (append (if (%current-target-system)
			      (list pkg-config-for-build wayland) '())
			  (list pkg-config
				python-pytest)))
   (home-page "https://github.com/flacjacket/pywlroots")
   (synopsis "Python binding to the wlroots library using cffi.")
   (description "A Python binding to the wlroots library using cffi. The library uses pywayland to provide the Wayland bindings and python-xkbcommon to provide wlroots keyboard functionality.")
   (license license:ncsa)))

(define-public ghc-xcb-types
  (package
    (name "ghc-xcb-types")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (hackage-uri "xcb-types" version))
       (sha256
        (base32 "0qdfj4d83b1fjmlysqncgi65ldf3qnrsj4lync95mgbaq2kzxj2r"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "xcb-types")))
    (inputs (list ghc-xml))
    (home-page "http://community.haskell.org/~aslatter/code/xcb-types")
    (synopsis "Parse XML files used by the XCB project")
    (description
     "This package provides types which mirror the structures used in the XCB code
generation XML files and parses these XML files into Haskell data structures.")
    (license license:bsd-3)))

(define-public ghc-language-python
  (package
    (name "ghc-language-python")
    (version "0.5.8")
    (source
     (origin
       (method url-fetch)
       (uri (hackage-uri "language-python" version))
       (sha256
        (base32 "1mf3czvnh9582klv0c9g7pcn1wx4qjwpvhv8la6afaifv6y5lki2"))))
    (build-system haskell-build-system)
    (arguments
     `(#:cabal-revision ("2"
                         "024fn653gmxw4ndmqvg1d3lwmxbvrlllc9iw2zw0c3nkcgcv39sg")))
    (native-inputs (list ghc-alex ghc-happy))
    (inputs (list ghc-monads-tf ghc-utf8-string))
    (home-page "http://github.com/bjpop/language-python")
    (synopsis "Parse and pretty print Python code in Haskell")
    (description
     "@code{language-python} is a Haskell library for lexical analysis,
parsing and pretty printing Python code.  It supports versions 2.x and 3.x of
Python.")
    (license license:bsd-3)
    (properties '((upstream-name . "language-python")))))

(define xcffibgen
  (package
    (name "xcffibgen")
    (version "1.4.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/tych0/xcffib")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0wz6zlaqsmpw7ahaadpd7m5n5c5b2ya30xwsana4j5ljygdvzqp9"))))
    (build-system haskell-build-system)
    (native-inputs
     (list ghc-hunit ghc-test-framework ghc-test-framework-hunit))
    (inputs
     (list libxcb
           ghc
           ghc-filemanip
           ghc-split
           ghc-language-python
           ghc-attoparsec
           ghc-either
           ghc-optparse-applicative
           ghc-xcb-types))
    (home-page "https://github.com/tych0/xcffib")
    (synopsis "Build tool for python-xcbffib bindings")
    (description
     "This is an internal package that provides a build tool to
generate code for the @code{python-xcbffib} package.")
    (license license:expat)))

 (define-public python-xcffib-1-5
    (package
      (name "python-xcffib")
      (version "1.5.0")
      (source (package-source xcffibgen))
      (build-system pyproject-build-system)
      (native-inputs
       (list pkg-config which xcb-proto xcffibgen))
      (inputs
       (list libxcb))
      (propagated-inputs
       (list python-cffi ; used at run time
             python-six))
      (arguments
       (list
        ;; Tests seem to require version 3.12 of Python.
        #:tests? #f
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'build 'generate-bindings
              (lambda* (#:key inputs #:allow-other-keys)
                (substitute* "Makefile"
                  (("^GEN=.*")
                   (format #f "GEN=~a~%"
                           (search-input-file inputs "/bin/xcffibgen"))))
                (invoke "make" "xcffib")))
            (add-before 'build 'fix-libxcb-path
              (lambda* (#:key inputs #:allow-other-keys)
             (let ((libxcb (assoc-ref inputs "libxcb")))
               (substitute* '("xcffib/__init__.py")
                 (("soname = ctypes.util.find_library.*xcb.*")
                  (string-append "soname = \"" libxcb "/lib/libxcb.so\"\n")))
                (substitute* "xcffib/__init__.py"
                  (("ctypes\\.util\\.find_library\\(\"xcb\"\\)")
                   (format #f "~s"
                           (search-input-file inputs "/lib/libxcb.so.1"))))
		#t))))))
      (home-page "https://github.com/tych0/xcffib")
      (synopsis "XCB Python bindings")
      (description
       "Xcffib is a replacement for xpyb, an XCB Python bindings.  It adds
  support for Python 3 and PyPy.  It is based on cffi.")
      (license license:expat)))

(define-public python-cairocffi-1-7
  (package
    (inherit python-cairocffi)
    (name "python-cairocffi")
    (version "1.7.1")
    (source
     (origin
       ;; The PyPI archive does not include the documentation, so use Git.
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/Kozea/cairocffi")
	     (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "08ilpzf1027f5w461yx2cg0ky64pd41kclfklyn8n292dmc9sk4g"))))
    (propagated-inputs
     (list python-xcffib-1-5)) ; used at run time
    ))

(define-public qtile-with-wayland ;;to prevent name conflicts
  (package
   (name "qtile")
   (version "0.28.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "qtile" version))
     (sha256
      (base32 "1xdd1ckrxjkyr1z1cxr2h46479niwlsh9225bq9lwpzvg8bjxdl9"))))
   (build-system pyproject-build-system)
   (arguments
    (list
     ;; A lot of tests fail despite Xvfb and writable temp/cache space.
     #:tests? #f
     #:phases
     #~(modify-phases %standard-phases
		      ;;these libs will not be found otherwise.
		      (add-before 'build 'pre-build
				  (lambda* (#:key inputs #:allow-other-keys)
				    (let ((pixman (string-append
						   (assoc-ref inputs "pixman")
						   "/include"))
					  (libdrm (string-append
						   (assoc-ref inputs "libdrm")
						   "/include"))
					  (libxcb (string-append
						   (assoc-ref inputs "libxcb")
						   "/include")))
				      (setenv "C_INCLUDE_PATH"
					      (string-append pixman "/" "pixman-1" ":"
							     libdrm "/" "libdrm" ":"
							     libxcb "/" "libxcb" ":"
							     (or (getenv "C_INCLUDE_PATH")
								 "")))
				      (setenv "LD_LIBRARY_PATH" (string-append
								 (assoc-ref inputs "libxcb") "/lib")))))
		      (add-after 'unpack 'patch-paths
			(lambda* (#:key inputs #:allow-other-keys)
			  (substitute* "libqtile/pangocffi.py"
			    (("^(gobject = ffi.dlopen).*" all def)
			     (format #f "~a(~s)~%" def
				     (search-input-file inputs "/lib/libgobject-2.0.so.0")))
			    (("^(pango = ffi.dlopen).*" all def)
			     (format #f "~a(~s)~%" def
				     (search-input-file inputs "/lib/libpango-1.0.so.0")))
			    (("^(xcb = ffi.dlopen).*" all def)
			     (format #f "~a(~s)~%" def
				     (search-input-file inputs "/lib/libxcb.so.1")))
			    (("^(pangocairo = ffi.dlopen).*" all def)
			     (format #f "~a(~s)~%" def
				     (search-input-file
				      inputs "/lib/libpangocairo-1.0.so.0"))))))
		      (add-after 'install 'install-sessions
			(lambda* (#:key inputs outputs #:allow-other-keys)
			  (let* ((out (assoc-ref outputs "out"))
				 (xsessions (string-append out "/share/xsessions"))
				 (wayland-sessions (string-append out "/share/wayland-sessions"))
				 (qtile (string-append out "/bin/qtile start"))
				(qbin (string-append out "/bin/qtile"))
				 (libxcb (string-append (assoc-ref inputs "libxcb") "/lib"))
				  (lib (string-append out "/lib/")))
			    (setenv "LD_LIBRARY_PATH" (string-append
			   			       (assoc-ref inputs "libxcb") "/lib"))
			    (mkdir-p xsessions)
			    (mkdir-p wayland-sessions)
			    (mkdir-p lib)
			    (copy-file (search-input-file inputs "/lib/libxcb.so")
				       (string-append lib "libxcb.so"))
			    (copy-file "resources/qtile.desktop"
				       (string-append xsessions "/qtile.desktop"))
			    (copy-file "resources/qtile-wayland.desktop"
				       (string-append wayland-sessions "/qtile-wayland.desktop"))
			    (substitute*
				(string-append xsessions "/qtile.desktop")
			      (("qtile start") qtile))
			    (substitute*
				(string-append wayland-sessions "/qtile-wayland.desktop")
			      (("qtile start") qtile)))))
		      ;;mistakes pywayland-scanner for loadable module
		      (delete 'sanity-check)
		      (add-before 'check 'pre-check
				  (lambda* (#:key tests? #:allow-other-keys)
				    (when tests?
				      (setenv "HOME" "/tmp")
				      (system "Xvfb :1 &")
				      (setenv "DISPLAY" ":1")
				      (setenv "XDG_CACHE_HOME" "/tmp")))))))
   (inputs
    (list glib
	  gobject-introspection
	  pango
	  libdrm
	  libinput
	  libxkbcommon
	  libxcb
	  libxinerama
	  xcb-util
	  xcb-util-keysyms
	  xcb-util-wm
	  wayland
	  wlroots))
   (propagated-inputs
    (list libxcb
	  python-cairocffi-1-7
	  python-cffi
	  python-coverage
	  ;;python-dateutil
	  python-dbus-next
	  python-flake8-5
	  python-flake8-pyproject
	  python-imaplib2
	  python-ipykernel
	  python-iwlib
	  python-jupyter-console
	  python-keyring
	  ;;python-libcst
	  python-mpd2
	  python-numpydoc
	  python-pep8-naming-0-14-1
	  python-psutil
	  python-pulsectl
	  python-pulsectl-asyncio
	  python-pygobject
	  python-pytest
	  python-pytz
	  python-pywayland
	  python-pywlroots
	  python-pyxdg
	  ;;setprojectile
	  python-sphinx
	  python-sphinx-rtd-theme
	  python-xcffib-1-5
	  python-xdg
	  python-xkbcommon
	  python-xmltodict))
   (native-inputs
    (list pkg-config
	  python-flake8-5
	  python-pytest-cov
	  python-setuptools-scm
	  python-wheel
	  xorg-server-for-tests))
   (home-page "http://qtile.org")
   (synopsis "Hackable tiling window manager written and configured in Python")
   (description "Qtile is simple, small, and extensible.  It's easy to write
your own layouts, widgets, and built-in commands.")
   (license license:expat)))
;qtile-with-wayland
