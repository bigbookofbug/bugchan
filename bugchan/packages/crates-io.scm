(define-module (bugchan packages crates-io)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages linux)
  #:use-module (guix build-system cargo)
  #:use-module (guix utils)
  #:use-module ((guix licenses)
              #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages))


(define-public rust-libudev-sys-fixed
  (package
    (inherit rust-libudev-sys-0.1)
    (native-inputs (list pkg-config eudev))
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
		       ("rust-pkg-config" ,rust-pkg-config-0.3))))))
