(define-module (bugchan util helper-functions)
  #:export (flatten-list
	    remove-nils))

(define (flatten-list lst)
  "take LST as a list of lists and flatten all nested lists:
(flatten-list (list 1 2 (list 3 (list 4)) 5)) => (1 2 3 4 5)"
  (cond
   ((nil? lst) '())
   ((pair? lst) (append
		 (flatten-list (car lst))
		 (flatten-list (cdr lst))))
   (else (list lst))))

(define (remove-nils x)
  (filter (lambda (c)
	    (not (or (nil? c)
		     (unspecified? c))))
	  x))
